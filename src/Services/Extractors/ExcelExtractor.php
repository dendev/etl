<?php

namespace Dendev\Etl\Services\Extractors;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class ExcelExtractor extends AExtractor
{
    public function __construct($src, $fields, $args = [])
    {
        $this->_merge_args_and_defaults_values($args);
        $this->src = $src;
        $this->fields = $fields;
    }

    protected function _check_src_is_valid()
    {
        return $this->_check_file_is_valid($this->src);
    }

    protected function _read_src_datas()
    {
        $src = $this->src;
        $datas = false;

        // get
        $src_type = IOFactory::identify($src);
        $reader = IOFactory::createReader($src_type);
        $spreadsheet = $reader->load($src);

        $worksheet = $spreadsheet->getActiveSheet();
        $datas = $worksheet->toArray();

        // options
        $skip_first_row = $this->_get_arg_value('skip_first_row');
        if( $skip_first_row)
        {
            array_shift($datas);
        }

        return $datas;
    }
}
