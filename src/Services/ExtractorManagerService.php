<?php

namespace Dendev\Etl\Services;


use Dendev\Etl\Services\Extractors\ArrayExtractor;
use Dendev\Etl\Services\Extractors\CsvExtractor;
use Dendev\Etl\Services\Extractors\ExcelExtractor;
use Dendev\Etl\Services\Extractors\MultiArrayExtractor;
use Dendev\Etl\Traits\UtilService;
use Illuminate\Support\Carbon;

/**
 * Class ExtractorManagerService
 * @package Dendev\Extractor
 */
class ExtractorManagerService
{
    use UtilService;

    /**
     * Stupid test to check if service exist
     * @return bool
     */
    public function test_me()
    {
        return true;
    }

    /**
     * Create and run an specific extractor.
     *
     * Extractor is created based on $type and for $src.
     *
     * @param $type string with value : csv, excel, array, multi_array
     * @param $src array with values or string with file path
     * @param $fields array with infos about fields to extract
     * @return mixed
     * @throws \Exception
     */
    public function run($type, $src, $fields)
    {
        $datas = [];
        $ref_types = ['csv', 'excel', 'array', 'multi_array'];

        if( in_array($type, $ref_types) )
        {
            $extractor = $this->_make_extractor($type, $src, $fields);
            $datas = $extractor->run();
        }
        else
        {
            $ref_types_string = implode( ',', $ref_types);
            throw new \Exception("[Etl::ExtractorManagerService::run] Invalid extractor type ( $type ) use $ref_types_string");
        }

        return $datas;
    }

    /**
     *
     * Just create extractor
     *
     * @param $type string with value : csv, excel, array, multi_array
     * @param $src array with values or string with file path
     * @param $fields array with infos about fields to extract
     * @return mixed
     * @throws \Exception
     */
    private function _make_extractor($type, $src, $fields)
    {
        $extractor = false;
        $ref_classes = [
            'array' => ArrayExtractor::class,
            'multi_array' => MultiArrayExtractor::class,
            'csv' => CsvExtractor::class,
            'excel' => ExcelExtractor::class,
        ];

        if( array_key_exists($type, $ref_classes) )
        {
            $classname = $ref_classes[$type];
            $extractor = new $classname($src, $fields);
        }
        else
        {
            throw new \Exception("[Etl::ExtractorMaangerService::_make_extractor] No class found to make $type extractor");
        }

        return $extractor;
    }
}
