<?php

namespace Tests\Unit;

use Dendev\Etl\Services\Extractors\ArrayExtractor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class ArrayExtractorManagerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Etl\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    public function testRun()
    {
        $fields = [
            't1' => [
                'name' => 'f01',
                'rules'=> ['required']
            ],
        ];

        $attemps = [
            ['t1' => 'test1']
        ];

        $array_extractor = $this->_make_basic_extractor(false, $fields);
        $datas = $array_extractor->run();
        $this->assertEquals($attemps, $datas);
    }

    //
    private function _make_basic_extractor($src = false, $fields = false)
    {
        if( ! $src)
            $src = [
                'f01' => 'test1',
                'f02' => 'test2'
            ];

        if( ! $fields)
            $fields = [
                't1' => [
                    'name' => 'f01',
                    'rules'=> ['required']
                ],
            ];

        $array_extractor = new ArrayExtractor( $src, $fields);

        return $array_extractor;
    }
}
