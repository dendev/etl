<?php

namespace Dendev\Etl\Services\Loaders;


class CsvLoader extends ALoader
{
    public function __construct($datas, $fields, $args = [])
    {
        parent::__construct($datas, $fields, $args);
    }

    protected function _check_args_is_valid()
    {
        $is_valid = false;
        $args = $this->_args;

        if( is_array($args) && array_key_exists('type', $args) && array_key_exists('path', $args) && array_key_exists('filename', $args))
        {
            $path = $args['path'];

            if( is_dir($path) && is_writable($path) )
            {
                $is_valid = true;
            }
            else
            {
                \Log::error("[Etl::CsvLoader::_check_args_is_valid] $path not writable directory", [
                    'path' => $path,
                    'args' => $args
                ]);
            }
        }
        else
        {
            \Log::error("[Etl::CsvLoader::_check_args_is_valid] Missing arg type, path or filename", [
                'args' => $args
            ]);
        }

        return $is_valid;
    }

    protected function _write_datas($datas)
    {
        $args = $this->_args;

        $path_with_filename = $args['path'] . '/' . $args['filename'];
        $fp = fopen($path_with_filename, 'w');

        foreach ($datas as $fields)
        {
            fputcsv($fp, $fields);
        }
        fclose($fp);

        return true;
    }
}
