<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class LoaderManagerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Etl\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    //
    public function testBasic()
    {
        $exist = \LoaderManager::test_me();
        $this->assertTrue($exist);
    }

    public function testCsv()
    {
        $datas = [
            ['t1' => 'test 01 ', 't2' => 'test 02' , 't3' => 'test 03'],
            ['t1' => 'test 11 ', 't2' => 'test 12' , 't3' => 'test 13'],
            ['t1' => 'test 21 ', 't2' => 'test 22' , 't3' => 'test 23'],
        ];

        $fields = [
            't1' => [
                'name' => 'f_1',
            ],
            't3' => [
                'name' => 'f_2',
            ],
        ];

        $args = [
            'type' => 'csv',
            'path' => __DIR__ . "/_files",
            'filename' => 'test_loader.csv'

        ];


        $is_loaded = \LoaderManager::run('csv', $datas, $fields, $args);
        $this->assertTrue($is_loaded);
    }

    public function testExcel()
    {
        $datas = [
            ['t1' => 'test 01 ', 't2' => 'test 02' , 't3' => 'test 03'],
            ['t1' => 'test 11 ', 't2' => 'test 12' , 't3' => 'test 13'],
            ['t1' => 'test 21 ', 't2' => 'test 22' , 't3' => 'test 23'],
        ];

        $fields = [
            't1' => [
                'name' => 'f_1',
            ],
            't3' => [
                'name' => 'f_2',
            ],
        ];

        $args = [
            'type' => 'csv',
            'path' => __DIR__ . "/_files",
            'filename' => 'test_loader.xlsx'

        ];


        $is_loaded = \LoaderManager::run('excel', $datas, $fields, $args);
        $this->assertTrue($is_loaded);
    }

    public function testMultiArray()
    {
        $datas = [
            ['t1' => 'test 01', 't2' => 'test 02' , 't3' => 'test 03'],
            ['t1' => 'test 11', 't2' => 'test 12' , 't3' => 'test 13'],
            ['t1' => 'test 21', 't2' => 'test 22' , 't3' => 'test 23'],
        ];

        $fields = [
            't3' => [
                'name' => 'f_2',
            ],
            't1' => [
                'name' => 'f_1',
            ],
        ];

        $args = [];

        $datas = \LoaderManager::run('multi_array', $datas, $fields, $args);
        $this->assertEquals([ 'f_2' => "test 03", 'f_1' => "test 01"], $datas[0]);
    }
}
