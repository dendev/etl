<?php

namespace Tests\Unit;

use Dendev\Etl\Services\Extractors\ArrayExtractor;
use Dendev\Etl\Services\Extractors\MultiArrayExtractor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class MultiArrayExtractorManagerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Etl\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    public function testRun()
    {
        $attemps = [
            ['t1' => 'test-r1a', 't2' => 'test-r1c'],
            ['t1' => 'test-r2a', 't2' => 'test-r2c'],
            ['t1' => 'test-r3a', 't2' => 'test-r3c'],
            ['t1' => 'test-r4a', 't2' => 'test-r4c'],
            ['t1' => 'test-r5a', 't2' => 'test-r5c'],
            ['t1' => 'test-r6a', 't2' => 'test-r6c'],
        ];

        $array_extractor = $this->_make_basic_extractor();
        $datas = $array_extractor->run();
        $this->assertEquals($attemps, $datas);
    }

    //
    private function _make_basic_extractor($src = false, $fields = false)
    {
        if( ! $src)
            $src = [
                'r1' => [ 'a' => 'test-r1a', 'b'  => 'test-r1b', 'c' => 'test-r1c', 'd' => 'test-r1d'],
                'r2' => [ 'a' => 'test-r2a', 'b'  => 'test-r2b', 'c' => 'test-r2c', 'd' => 'test-r2d'],
                'r3' => [ 'a' => 'test-r3a', 'b'  => 'test-r3b', 'c' => 'test-r3c', 'd' => 'test-r3d'],
                'r4' => [ 'a' => 'test-r4a', 'b'  => 'test-r4b', 'c' => 'test-r4c', 'd' => 'test-r4d'],
                'r5' => [ 'a' => 'test-r5a', 'b'  => 'test-r5b', 'c' => 'test-r5c', 'd' => 'test-r5d'],
                'r6' => [ 'a' => 'test-r6a', 'b'  => 'test-r6b', 'c' => 'test-r6c', 'd' => 'test-r6d'],
            ];

        if( ! $fields)
            $fields = [
                't1' => [
                    'name' => 'a',
                    'rules'=> ['required']
                ],
                't2' => [
                    'name' => 'c',
                    'rules'=> ['required']
                ],
            ];

        $multi_array_extractor = new MultiArrayExtractor( $src, $fields);

        return $multi_array_extractor;
    }
}
