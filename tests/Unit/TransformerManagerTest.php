<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class TransformerManagerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Etl\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    //
    public function testBasic()
    {
        $exist = \TransformerManager::test_me();
        $this->assertTrue($exist);
    }

    public function testTrim()
    {
        $datas = [
            ['t1' => 'Petasites hybridus ', 't2' => 'Pétasite officinal'],
            ['t1' => ' Tussilago farfara', 't2' => 'Tussilage '],
            ['t1' => 'Cirsium arvense  ', 't2' => 'Cirse des    champs  '],
        ];

        $transforms = [
            't1' => [['type' => 'trim']],
            't2' => [['type' => 'trim']],
        ];

        $attemp = [
            ['t1' => 'Petasites hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage'],
            ['t1' => 'Cirsium arvense', 't2' => 'Cirse des    champs'],
        ];

        $transformed = \TransformerManager::run($transforms, $datas);

        $this->assertEquals($attemp[0],$transformed[0]);
    }

    public function testUcfirst()
    {
        $datas = [
            ['t1' => 'petasites hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'tussilago farfara', 't2' => 'tussilage'],
            ['t1' => 'cirsium arvense', 't2' => 'cirse des champs'],
        ];

        $transforms = [
            't1' => [[ 'type' => 'ucfirst']],
            't2' => [[ 'type' => 'ucfirst']]
        ];

        $attemp = [
            ['t1' => 'Petasites hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage'],
            ['t1' => 'Cirsium arvense', 't2' => 'Cirse des champs'],
        ];

        $transformed = \TransformerManager::run($transforms, $datas);

        $this->assertEquals($attemp[0],$transformed[0]);
        $this->assertEquals($attemp[1],$transformed[1]);
        $this->assertEquals($attemp[2],$transformed[2]);
    }

    public function testReplace()
    {
        $datas = [
            ['t1' => 'Petasites hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage'],
            ['t1' => 'Cirsium arvense', 't2' => 'Cirse des champs'],
        ];

        $transforms = [
            't1' => [
                [
                    'type' => 'replace',
                    'args' => [
                        'Petasites' => 'Petale',
                        'farfara' => 'Farfadet',
                        'Cirse' => 'Criquet',
                    ]
                ]
            ],
            't2' => [
                [
                    'type' => 'replace',
                    'args' => [
                        'farfara' => 'Farfadet',
                        'Cirse' => 'Criquet',
                    ]
                ]
            ]
        ];


        $attemp = [
            ['t1' => 'Petale hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'Tussilago Farfadet', 't2' => 'Tussilage'],
            ['t1' => 'Cirsium arvense', 't2' => 'Criquet des champs'],
        ];

        $transformed = \TransformerManager::run($transforms, $datas);

        $this->assertEquals($attemp[0],$transformed[0]);
        $this->assertEquals($attemp[1],$transformed[1]);
        $this->assertEquals($attemp[2],$transformed[2]);
    }

    public function testRemove()
    {
        $datas = [
            ['t1' => 'Petasites hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage'],
            ['t1' => 'Cirsium arvense', 't2' => 'Cirse des champs'],
        ];

        $transforms = [
            't1' => [
                [
                    'type' => 'remove',
                    'args' => [
                        'Petasites',
                        'Cirse',
                    ]
                ]
            ],
            't2' => [
                [
                    'type' => 'remove',
                    'args' => [
                        'Petasites',
                        'Cirse',
                    ]
                ]
            ],
        ];

        $attemp = [
            ['t1' => ' hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage'],
            ['t1' => 'Cirsium arvense', 't2' => ' des champs'],
        ];

        $transformed = \TransformerManager::run($transforms, $datas);

        $this->assertEquals($attemp[0],$transformed[0]);
        $this->assertEquals($attemp[1],$transformed[1]);
        $this->assertEquals($attemp[2],$transformed[2]);
    }

    public function testImplode()
    {
        $datas = [
            ['t1' => 'Petasites hybridus', 't2' => 'Pétasite officinal', 't3' => 'n 1'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage', 't3' => 'n 2'],
            ['t1' => 'Cirsium arvense', 't2' => 'Cirse des champs', 't3' => 'n 3'],
        ];

        $transforms = [
            't1' => [
                [
                    'type' => 'implode',
                    'args' => [
                        'separator' => ', ',
                        'fields' => ['t1', 't3'],
                        'field' => 't13',
                    ]
                ]
            ]
        ];

        $attempts = [
            ['t1' => 'Petasites hybridus', 't2' => 'Pétasite officinal', 't3' => 'n 1', 't13' => 'Petasites hybridus, n 1'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage', 't3' => 'n 2', 't13' => 'Tussilago farfara, n 2'],
            ['t1' => 'Cirsium arvense', 't2' => 'Cirse des champs', 't3' => 'n 3', 't13' => 'Cirsium arvense, n 3'],
        ];


        $transformed = \TransformerManager::run($transforms, $datas);

        $this->assertEquals($attempts[0],$transformed[0]);
        $this->assertEquals($attempts[1],$transformed[1]);
        $this->assertEquals($attempts[2],$transformed[2]);
    }

    public function testTrimAndUcfirst()
    {
        $datas = [
            ['t1' => ' petasites hybridus ', 't2' => 'Pétasite officinal'],
            ['t1' => ' Tussilago farfara ', 't2' => 'tussilage '],
            ['t1' => 'Cirsium arvense  ', 't2' => 'cirse des    champs  '],
        ];

        $attemp = [
            ['t1' => 'Petasites hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage'],
            ['t1' => 'Cirsium arvense', 't2' => 'Cirse des    champs'],
        ];

        $transforms = [
            't1' => [
                ['type' => 'trim', 'args' => []],
                ['type' => 'ucfirst'],
            ],
            't2' => [
                ['type' => 'trim', 'args' => []],
                ['type' => 'ucfirst'],
            ]
        ];

        $transformed = \TransformerManager::run($transforms, $datas, );

        $this->assertEquals($attemp[0],$transformed[0]);
        $this->assertEquals($attemp[1],$transformed[1]);
        $this->assertEquals($attemp[2],$transformed[2]);
    }

    public function testEmptyTransforms()
    {
        $datas = [
            ['t1' => ' petasites hybridus ', 't2' => 'Pétasite officinal'],
            ['t1' => ' Tussilago farfara ', 't2' => 'tussilage '],
            ['t1' => 'Cirsium arvense  ', 't2' => 'cirse des    champs  '],
        ];


        $transforms = [
        ];

        $transformed = \TransformerManager::run($transforms, $datas, );

        $this->assertEquals($datas[0],$transformed[0]);
        $this->assertEquals($datas[1],$transformed[1]);
        $this->assertEquals($datas[2],$transformed[2]);
    }
}
