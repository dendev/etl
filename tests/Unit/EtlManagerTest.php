<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class EtlManagerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Etl\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    //
    public function testBasic()
    {
        $exist = \EtlManager::test_me();
        $this->assertTrue($exist);
    }

    public function testExcelToArray()
    {
        $extractor = [
            'type' => 'excel',
            'src' => __DIR__ . "/_files/etl_test.xlsx",
            'fields' => [
                't1' => [
                    'name' => 1,
                    'rules'=> ['required']
                ],
                't2' => [
                    'name' => 2,
                    'rules'=> ['required']
                ],
            ],
        ];

        $transformer = [
            't1' => [
                ['type' => 'trim'],
                ['type' => 'ucfirst', 'args' => []]
            ],
            't2' => [
                ['type' => 'trim'],
                ['type' => 'ucfirst', 'args' => []]
            ]
        ];

        $loader = [
            'type' => 'multi_array',
            'fields' => [
                't1' => [
                    'name' => 'f_1',
                ],
                't2' => [
                    'name' => 'f_2',
                ],
            ]
        ];

        $attempts = [
            ["f_1" => "Petasites hybridus", "f_2" => "Pétasite officinal"],
            ["f_1" => "Tussilago farfara", "f_2" => "Tussilage"],
            ["f_1" => "Cirsium arvense", "f_2" => "Cirse des champs"]
        ];

        $datas = \EtlManager::run($extractor, $transformer, $loader);
        $this->assertEquals($attempts[0], $datas[0]);
        $this->assertEquals($attempts[1], $datas[1]);
        $this->assertEquals($attempts[2], $datas[2]);
    }
}
