<?php

namespace Dendev\Etl\Facades;

use Illuminate\Support\Facades\Facade;

class LoaderManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'loader_manager';
    }
}
