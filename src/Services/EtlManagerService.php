<?php

namespace Dendev\Etl\Services;


use Dendev\Etl\Traits\UtilService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\Console\Exception\CommandNotFoundException;

/**
 * Class EtlManagerService
 * @package Dendev\Etl
 */
class EtlManagerService
{
    use UtilService;

    /**
     * check if service is accessible
     * @return bool
     */
    public function test_me()
    {
        return true;
    }

    /**
     * run full etl with settings passed in args
     * @param $extractor where to get raw datas
     * @param $transformers hot to transforms raw datas
     * @param $loader where to put final datas
     * @return array
     */
    public function run($extractor, $transformers, $loader)
    {
        $datas = [];

        $is_valid_extractor = $this->_check_extractor($extractor);
        $is_valid_transformers = $this->_check_transformers($transformers);
        $is_valid_loader = $this->_check_loader($loader);

        if( $is_valid_extractor && $is_valid_transformers && $is_valid_loader )
        {
            // Extract
            $args = array_key_exists('args', $extractor) ? $extractor['args'] : [];
            $datas = \ExtractorManager::run($extractor['type'], $extractor['src'], $extractor['fields'], $args);

            // Transform
            //-- args in transformers && automatic set if not provided
            $datas = \TransformerManager::run($transformers, $datas);

            // Load
            $args = array_key_exists('args', $extractor) ? $extractor['args'] : [];
            $datas = \LoaderManager::run($loader['type'], $datas, $loader['fields'], $args);
        }

        return $datas;
    }

    /**
     * Basic check extractor args validity
     *
     * @param $extractor must be an array with keys: type, src, fields
     * @return bool
     */
    private function _check_extractor($extractor)
    {
        $is_valid = false;

        if( is_array($extractor)
            && array_key_exists('type', $extractor)
            && array_key_exists('src', $extractor)
            && array_key_exists('fields', $extractor) )
        {
            $is_valid = true;
        }
        else
        {
            \Log::error("[EtlManager::_check_extractor] Bad extractor format", [
                'extractor' => $extractor
            ]);
        }

        return $is_valid;
    }

    /**
     * Basic check transformer args validity
     *
     * @param $transformers must be an array of array
     * @return bool
     */
    private function _check_transformers($transformers)
    {
        $is_valid = false;

        if( is_array($transformers) )
        {
            $is_valid = true;
        }
        else
        {
            \Log::error("[EtlManager::_check_transformers] Bad transformer format", [
                'transformers' => $transformers
            ]);
        }

        return $is_valid;
    }

    /**
     * Basic check loader args validity
     *
     * @param $loader must be an array with keys: type, fields
     * @return bool
     */
    private function _check_loader($loader)
    {
        $is_valid = false;

        if( is_array($loader) && array_key_exists('type', $loader) && array_key_exists('fields', $loader) )
        {
            $is_valid = true;
        }
        else
        {
            \Log::error("[EtlManager::_check_loader] Bad loader format", [
                'loader' => $loader
            ]);
        }


        return $is_valid;
    }
}
