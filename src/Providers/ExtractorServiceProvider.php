<?php

namespace Dendev\Etl\Providers;

use Dendev\Etl\Services\ExtractorManagerService;
use Illuminate\Support\ServiceProvider;

class ExtractorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('extractor_manager', function ($app) {
            return new ExtractorManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
