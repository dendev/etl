<?php

namespace Dendev\Etl\Services\Transformers;

/**
 * Class ATransformer
 * @package Dendev\Etl\Services\Transformers
 */
abstract class ATransformer
{

    /**
     * ATransformer constructor.
     * @param $datas
     * @param $args
     */
    public function __construct($datas, $fields, $args)
    {
        $this->_datas = $datas;
        $this->_fields = $fields;
        $this->_args = $args;
    }

    /**
     * @return array|false|mixed
     */
    public function run()
    {
        $ok = false;

        $datas = $this->_datas;
        $fields = $this->_fields;
        $args = $this->_args;

        // check
        $args_are_valid = $this->_check_args_are_valid();

        if( $args_are_valid)
        {
            // transform
            $ok = $this->_transform($datas, $fields, $args);

            // inform
            if( ! $ok )
                \Log::error("[Etl::ATransformer::run] transformer failed", []);
        }

        return $ok;
    }

    protected function _check_args_are_valid()
    {
        $is_valid = true;
        return $is_valid;
    }

    protected function _transform($datas, $fields, $args)
    {
        $funct = function ($data){
            return $data;
        };

        return $this->_iterate_and_apply($datas, $funct, $fields, $args);
    }

    protected function _iterate_and_apply(&$datas, $funct, $fields, $args = [])
    {
        if( is_array($datas) )
        {
            foreach( $datas as $key_data => $data )
            {
                if( is_array($data) )
                {
                    foreach( $data as $key_dt => $dt )
                    {
                        if( is_array( $dt ) )
                        {
                            \Log::warning("[Etl::ATransformer::_iterate_and_apply] So deep array", [
                            ]);
                        }
                        else
                        {
                            if( in_array($key_dt, $fields))
                            {
                                $formated = $funct($dt, $key_data, $key_dt, $args);
                                $datas[$key_data][$key_dt] = $formated;
                            }
                        }
                    }
                }
                else
                {
                    $formated = $funct($data, $args);
                    $datas[$key_data] = $formated;
                }
            }
        }
        else
        {
            $formated = $funct($datas, $args);
            $datas = $formated;
        }

        return $datas;
    }
}
