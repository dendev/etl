<?php

namespace Dendev\Etl\Services;


use Dendev\Etl\Services\Loaders\JsonLoader;
use Dendev\Etl\Services\Loaders\MultiArrayLoader;
use Dendev\Etl\Services\Loaders\CsvLoader;
use Dendev\Etl\Services\Loaders\ExcelLoader;
use Dendev\Etl\Traits\UtilService;

/**
 * Class LoaderManagerService
 *
 * Load datas in file or var
 *
 * @package Dendev\Etl\Services
 */
class LoaderManagerService
{
    use UtilService;

    /**
     * just check if service is accessible
     * @return bool
     */
    public function test_me()
    {
        return true;
    }

    /**
     * Run all utils method to load datas in ouptput
     *
     * Kind of output is defined by $type
     *
     * @param $type string with value csv, excel, multi_array or json
     * @param $datas array with all datas
     * @param $fields array with infos about field to load in output
     * @param $args array with infos about output src like file path, file name
     * @return mixed
     * @throws \Exception
     */
    public function run($type, $datas, $fields, $args)
    {
        $is_loaded = false;
        $ref_types = ['csv', 'excel', 'multi_array', 'json' ];

        if( in_array($type, $ref_types) )
        {
            if( $datas )
            {
                $loader = $this->_make_loader($type, $datas, $fields, $args);
                $is_loaded = $loader->run();
            }
            else
            {
                \Log::error("[Etl::LoaderManager::run] no datas to load", [
                    'datas' => $datas
                ]);
            }
        }
        else
        {
            $ref_types_string = implode( ',', $ref_types);
            throw new \Exception("[Etl::LoaderManagerService::run] Invalid loader type ( $type ) use $ref_types_string");
        }

        return $is_loaded;
    }

    /**
     * Create a specific loader for type asked
     *
     * @param $type string with value csv, excel, multi_array or json
     * @param $datas array with all datas
     * @param $fields array with infos about field to load in output
     * @param $args array with infos about output src like file path, file name
     * @return mixed
     * @throws \Exception
     */
    private function _make_loader($type, $datas, $fields, $args = [])
    {
        $loader = false;
        $ref_classes = [
            'csv' => Csvloader::class,
            'excel' => Excelloader::class,
            'multi_array' => MultiArrayloader::class,
            'json' => JsonLoader::class,
        ];

        if( array_key_exists($type, $ref_classes) )
        {
            $classname = $ref_classes[$type];
            $loader = new $classname($datas, $fields, $args);
        }
        else
        {
            throw new \Exception("[Etl::LoaderManagerService::_make_loader] No class found to make $type loader");
        }

        return $loader;
    }
}
