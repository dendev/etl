# Etl

> Help to extract datas and transforms it

# Install
```bash
composer config repositories.etl vcs https://gitlab.com/dendev/etl.git
composer require dendev/etl
```

# Usage

## Extractor

> Extract some datas from src.

**Type** : can be array, csv, excel, multi_array.  
**Src** :  path to file or array of datas.  
**Fields** : indict which fields should be extracted. 

Ex: extract all f01 and f04 from basic array. Extracted field will have t1 and t2 index in extracted array
```php
<?php
$src = [
        'f01' => 'test1',
        'f02' => 'test2',
        'f03' => 'test3',
        'f04' => 'test4',
    ];

$fields = [
    't1' => [
        'name' => 'f01',
        'rules'=> ['required']
    ],
    't2' => [
        'name' => 'f04',
        'rules'=> ['required']
    ],
];

$attemps = [
    't1' => 'test1',
    't2' => 'test4',
];

$datas = \ExtractorManager::run('array', $src, $fields);
?>
```

## Transformer

> Transforms datas in array

**Datas** : array with values ( result of extractor ).  
**Transforms**: indic how to transforms value in field.  
Available transforms are : implode, remove, replace, trim, ucfirst.

Ex: Ucfirst on t1 and t2 values
```php
$datas = [
    ['t1' => 'petasites hybridus', 't2' => 'Pétasite officinal'],
    ['t1' => 'tussilago farfara', 't2' => 'tussilage'],
    ['t1' => 'cirsium arvense', 't2' => 'cirse des champs'],
];

$transforms = [
    't1' => [[ 'type' => 'ucfirst']],
    't2' => [[ 'type' => 'ucfirst', 'args' => []]]
];

$attempts = [
    ['t1' => 'Petasites hybridus', 't2' => 'Pétasite officinal'],
    ['t1' => 'Tussilago farfara', 't2' => 'Tussilage'],
    ['t1' => 'Cirsium arvense', 't2' => 'Cirse des champs'],
];

$transformed = \TransformerManager::run($transforms, $datas);
```

Some transforms need args ( like implode, remove and replace). 
```php 
[
    'type' => 'implode',
    'args' => [
        'separator' => ', ',
        'fields' => ['t1', 't3'],
        'field' => 't13',
    ]
]
```

```php
[
    'type' => 'remove',
    'args' => [
        'Petasites',
        'Cirse',
    ]
]
```

```php
[
    'type' => 'replace',
    'args' => [
        'farfara' => 'Farfadet',
        'Cirse' => 'Criquet',
    ]
]
```


## Loader

> Put array datas in output source.

**Type**: Kind of output, can be csv, excel, json, multi_array
**Fields** : key from array who will be renamed ( with value of name key) en send in output.  
**Args**: array with details for file output

```php
$datas = [
    ['t1' => 'test 01 ', 't2' => 'test 02' , 't3' => 'test 03'],
    ['t1' => 'test 11 ', 't2' => 'test 12' , 't3' => 'test 13'],
    ['t1' => 'test 21 ', 't2' => 'test 22' , 't3' => 'test 23'],
];

$fields = [
    't1' => [
        'name' => 'f_1',
    ],
    't3' => [
        'name' => 'f_2',
    ],
];

$args = [
    'type' => 'csv',
    'path' => __DIR__ . "/_files",
    'filename' => 'test_loader.csv'
];


$is_loaded = \LoaderManager::run('csv', $datas, $fields, $args)
```

## Etl

> run extract, transform and load

**Extractor** : Full extractor info with type, src and fields 
**Transformer**: All transforms for each fields
**Loader** : Full loader info with type and fields

Ex: extract col 1 and 2 from csv, trim and ucfirst values and output all in array with f_1 and f_2 key
```php
$extractor = [
    'type' => 'excel',
    'src' => __DIR__ . "/_files/etl_test.xlsx",
    'fields' => [
        't1' => [
            'name' => 1,
            'rules'=> ['required']
        ],
        't2' => [
            'name' => 2,
            'rules'=> ['required']
        ],
    ],
];

$transformer = [
    't1' => [
        ['type' => 'trim'],
        ['type' => 'ucfirst', 'args' => []]
    ],
    't2' => [
        ['type' => 'trim'],
        ['type' => 'ucfirst', 'args' => []]
    ]
];

$loader = [
    'type' => 'multi_array',
    'fields' => [
        't1' => [
            'name' => 'f_1',
        ],
        't2' => [
            'name' => 'f_2',
        ],
    ]
];

$attempts = [
    ["f_1" => "Petasites hybridus", "f_2" => "Pétasite officinal"],
    ["f_1" => "Tussilago farfara", "f_2" => "Tussilage"],
    ["f_1" => "Cirsium arvense", "f_2" => "Cirse des champs"]
];

$datas = \EtlManager::run($extractor, $transformer, $loader);
```
