<?php

namespace Dendev\Etl;

use Illuminate\Support\ServiceProvider;

class AddonServiceProvider extends ServiceProvider
{
    use AutomaticServiceProvider;

    protected $vendorName = 'dendev';
    protected $packageName = 'etl';
    protected $commands = [];
}
