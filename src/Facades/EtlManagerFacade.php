<?php

namespace Dendev\Etl\Facades;

use Illuminate\Support\Facades\Facade;

class EtlManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'etl_manager';
    }
}
