<?php


namespace Dendev\Etl\Services\Loaders;


/**
 * Utils and generic method to load datas in file or var
 * Class ALoader
 * @package Dendev\Etl\Services\Loaders
 */
abstract class ALoader
{
    /**
     * ALoader constructor.
     * @param $src
     * @param $fields
     * @param array $args
     */
    public function __construct($datas, $fields, $args)
    {
        $this->_datas = $datas;
        $this->_fields = $fields;
        $this->_args = $args;
    }

    public function run()
    {
        $ok = false;

        // check
        $src_is_valid = $this->_check_args_is_valid();

        if( $src_is_valid)
        {
            // filter
            $datas = $this->_filter_datas();

            // write
            $ok = $this->_write_datas($datas);

            // inform
            if( ! $ok )
                \Log::error("[Etl::ALoader::run] loader failed", []);
        }

        return $ok;
    }

    protected function _check_args_is_valid()
    {
        $is_valid = true;
        return $is_valid;
    }

    protected function _filter_datas()
    {
        $datas = $this->_datas;
        $fields = $this->_fields;

        $filtered = [];

        foreach( $datas as $data )
        {
            // get datas listed in field
            $row = [];
            foreach( $fields as $key => $field )
            {
                if( array_key_exists($key, $data) )
                {
                    $row[$field['name']] = $data[$key];
                }
            }

            // sort
            //ksort($row);

            // save
            if( count($row) > 0 )
            {
                $filtered[] = $row;
            }
        }


        return $filtered;
    }

    protected function _write_datas($datas)
    {
        return $datas;
    }
}

