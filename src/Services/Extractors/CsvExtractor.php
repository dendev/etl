<?php

namespace Dendev\Etl\Services\Extractors;


class CsvExtractor extends AExtractor
{
    public function __construct($src, $fields, $args = [])
    {
        $this->_merge_args_and_defaults_values($args);
        $this->src = $src;
        $this->fields = $fields;
    }

    protected function _check_src_is_valid()
    {
        return $this->_check_file_is_valid($this->src);
    }

    protected function _read_src_datas()
    {
        $src = $this->src;
        $datas = false;

        // get
        $row = 1;
        if (($handle = fopen($src, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $datas[] = $data;
                $row++;
            }
            fclose($handle);
        }

        // options
        $skip_first_row = $this->_get_arg_value('skip_first_row');
        if( $skip_first_row)
        {
           array_shift($datas);
        }

        return $datas;
    }
}
