<?php

namespace Tests\Unit;

use Dendev\Etl\Services\Extractors\ArrayExtractor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class AExtractorManagerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Etl\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }


    public function testProtectedCheckSrcIsValid()
    {
        $extractor = $this->_make_basic_extractor();
        $is_valid = $this->_invokeMethod($extractor, '_check_src_is_valid');

        $this->assertTrue($is_valid);
    }

    public function testProtectedCheckFieldsAreValid()
    {
        $extractor = $this->_make_basic_extractor();
        $is_valid = $this->_invokeMethod($extractor, '_check_fields_are_valid');

        $this->assertTrue($is_valid);
    }

    public function testProtectedCheckFieldIsValid()
    {
        $field = [
            'name' => 2,
            'rules'=> ['required']
        ];
        $extractor = $this->_make_basic_extractor();
        $is_valid = $this->_invokeMethod($extractor, '_check_field_is_valid', [$field]);

        $this->assertTrue($is_valid);
    }

    //
    private function _invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    private function _make_basic_extractor($src = false, $fields = false)
    {
        if( ! $src)
            $src = [
                'f01' => 'test1',
                'f02' => 'test2'
            ];

        if( ! $fields)
            $fields = [
                't1' => [
                    'name' => 'f01',
                    'rules'=> ['required']
                ],
            ];

        $array_extractor = new ArrayExtractor( $src, $fields);

        return $array_extractor;
    }
}
