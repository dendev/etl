<?php

namespace Dendev\Etl\Providers;

use Dendev\Etl\Services\TransformerManagerService;
use Illuminate\Support\ServiceProvider;

class TransformerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('transformer_manager', function ($app) {
            return new TransformerManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
