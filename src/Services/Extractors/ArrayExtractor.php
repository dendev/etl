<?php

namespace Dendev\Etl\Services\Extractors;

class ArrayExtractor extends AExtractor
{
    public function __construct($src, $fields, $args = [])
    {
        $this->src = $src;
        $this->fields = $fields;
        $this->args = $args;
    }

    public function run()
    {
        $values = [];

        // check
        $src_is_valid = $this->_check_src_is_valid();
        $fields_are_valid = $this->_check_fields_are_valid();

        // raw datas
        $datas = $this->_read_src_datas();

        // do or log
        if ($src_is_valid && $fields_are_valid)
            $values = $this->_get_values($datas);
        else if (!$src_is_valid)
            \Log::error("[Etl::ArrayExtractor::run] Invalid src provided", ['src' => $this->src, 'src_is_valid' => $src_is_valid]);
        else
            \Log::error("[Etl::ArrayExtractor::run] Invalid fields provided", ['src' => $this->src, 'fields_are_valid' => $fields_are_valid]);

        return $values;
    }

    protected function _check_src_is_valid()
    {
        $is_valid = false;

        $src = $this->src;
        if ($src && is_array($src) && count($src) > 0)
            $is_valid = true;

        return $is_valid;
    }

    protected function _read_src_datas()
    {
        $src = $this->src;
        $datas = false;

        $datas = [$src];

        return $datas;
    }
}
