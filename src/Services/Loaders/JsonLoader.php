<?php

namespace Dendev\Etl\Services\Loaders;


class JsonLoader extends ALoader
{
    public function __construct($datas, $fields, $args = [])
    {
        parent::__construct($datas, $fields, $args);
    }

    protected function _check_args_is_valid()
    {
        $is_valid = true;
        return $is_valid;
    }

    protected function _write_datas($datas)
    {
        $datas = parent::_write_datas($datas);

        return json_encode($datas, true);
    }
}
