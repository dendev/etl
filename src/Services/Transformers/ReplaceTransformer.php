<?php

namespace Dendev\Etl\Services\Transformers;


class ReplaceTransformer extends ATransformer
{
    public function __construct($datas, $fields, $args = [])
    {
        parent::__construct($datas, $fields, $args);
    }

    protected function _check_args_are_valid()
    {
        $is_valid = false;

        $args = $this->_args;

        if( is_array($args) )
        {
            $is_valid = true;
        }


        return $is_valid;
    }

    protected function _transform($datas, $fields, $args)
    {
        $replaces = $args;
        $funct = function ($data, $key_row, $key_field, $replaces)
        {
            $formated = $data;
            foreach( $replaces as $search => $replace )
            {
                $formated = str_replace($search, $replace, $formated);
            }
            return $formated;
        };

        return $this->_iterate_and_apply($datas, $funct, $fields, $replaces);
    }
}
