<?php

namespace Dendev\Etl\Facades;

use Illuminate\Support\Facades\Facade;

class TransformerManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'transformer_manager';
    }
}
