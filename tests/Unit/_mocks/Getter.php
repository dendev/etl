<?php

namespace Tests\Unit\Mocks;


class Getter
{
    public function get_1()
    {
        // do complex action to get actual value
        return 'a';
    }

    public function get_2()
    {
        return 99;
    }
}
