<?php

namespace Dendev\Etl\Services;


use Dendev\Etl\Services\Transformers\AmountTransformer;
use Dendev\Etl\Services\Transformers\ImplodeTransformer;
use Dendev\Etl\Services\Transformers\RemoveTransformer;
use Dendev\Etl\Services\Transformers\ReplaceTransformer;
use Dendev\Etl\Services\Transformers\TrimTransformer;
use Dendev\Etl\Services\Transformers\UcfirstTransformer;
use Dendev\Etl\Traits\UtilService;

/**
 * Class TransformManagerService
 * @package Dendev\Etl
 */
class TransformerManagerService
{
    use UtilService;

    /**
     * just check service accessibility
     * @return bool
     */
    public function test_me()
    {
        return true;
    }

    /**
     * Run create && run all transforms asked by $transforms
     *
     * @param $transforms array with all transform to do for each fields
     * @param $datas array with all datas to transform
     * @return mixed
     * @throws \Exception
     */
    public function run($transforms, $datas)
    {
        foreach( $transforms as $field => $transforms_for_field )
        {
            foreach ($transforms_for_field as $transform )
            {
                $type = $transform['type'];
                $args = array_key_exists('args', $transform) ? $transform['args'] : [];

                $ref_types = ['trim', 'ucfirst', 'replace', 'remove', 'implode', 'amount'];

                if (in_array($type, $ref_types))
                {
                    $transformer = $this->_make_transformer($type, $datas, [$field], $args);
                    $transformed = $transformer->run();
                    $datas = $transformed;
                }
                else
                {
                    $ref_types_string = implode(',', $ref_types);
                    throw new \Exception("[Etl::TransformerManagerService::run] Invalid transformer type ( $type ) use $ref_types_string");
                }
            }
        }

        return $datas;
    }

    /**
     * Create a specific transformer for type asked
     *
     * @param $transforms array with all transform to do for each fields
     * @param $datas array with all datas to transform
     * @param array $args
     * @return mixed
     * @throws \Exception
     */
    private function _make_transformer($type, $datas, $fields, $args = [])
    {
        $transformer = false;
        $ref_classes = [
            'trim' => TrimTransformer::class,
            'ucfirst' => UcfirstTransformer::class,
            'replace' => ReplaceTransformer::class,
            'remove' => RemoveTransformer::class,
            'implode' => ImplodeTransformer::class,
            'amount' => AmountTransformer::class,
        ];

        if( array_key_exists($type, $ref_classes) )
        {
            $classname = $ref_classes[$type];
            $transformer = new $classname($datas, $fields, $args);
        }
        else
        {
            throw new \Exception("[Etl::TransformerManagerService::_make_transformer] No class found to make $type transformer");
        }

        return $transformer;
    }
}
