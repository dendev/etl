<?php

namespace Dendev\Etl\Services\Transformers;


class TrimTransformer extends ATransformer
{
    public function __construct($datas, $fields, $args = [])
    {
        parent::__construct($datas, $fields, $args);
    }

    protected function _transform($datas, $fields, $args)
    {
        $funct = function ($data, $key_row, $key_field, $removes)
        {
            return trim($data);
        };

        return parent::_iterate_and_apply($datas, $funct, $fields, $args);
    }
}
