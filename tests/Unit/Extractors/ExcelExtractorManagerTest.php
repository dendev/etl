<?php

namespace Tests\Unit;

use Dendev\Etl\Services\Extractors\ExcelExtractor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class ExceltractorManagerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Etl\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    public function testRun()
    {
        $fields = [
            't1' => [
                'name' => 2,
                'rules'=> ['required']
            ],
            't2' => [
                'name' => 0,
                'rules'=> ['required']
            ],
        ];

        $attemps = [
            ['t1' => 'Pétasite officinal', 't2' => 'Asteraceae'],
            ['t1' => 'Tussilage', 't2' => 'Asteraceae'],
            ['t1' => 'Cirse des champs', 't2' => 'Asteraceae'],
        ];

        $extractor = $this->_make_basic_extractor(false, $fields);
        $datas = $extractor->run();
        $this->assertEquals($attemps, $datas);
    }

    //
    private function _make_basic_extractor($src = false, $fields = false)
    {
        if( ! $src)
            $src = __DIR__ . "/../_files/test.xlsx";

        if( ! $fields)
            $fields = [
                't1' => [
                    'name' => 'name_fr',
                    'rules'=> ['required']
                ],
            ];

        $extractor = new ExcelExtractor( $src, $fields);

        return $extractor;
    }
}
