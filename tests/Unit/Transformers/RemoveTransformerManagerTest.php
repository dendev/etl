<?php

namespace Tests\Unit;

use Dendev\Etl\Services\Transformers\RemoveTransformer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class RemoveTransformerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Etl\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    public function testRun()
    {
        $attempts = $this->_make_basic_attempts();
        $transformer = $this->_make_basic_transformer();

        $transformed = $transformer->run();

        $this->assertEquals($attempts[0],$transformed[0]);
        $this->assertEquals($attempts[1],$transformed[1]);
        $this->assertEquals($attempts[2],$transformed[2]);
    }

    //
    private function _make_basic_transformer($datas = false, $fields = false, $args = false)
    {
        if( ! $datas )
        $datas = [
            ['t1' => 'Petasites hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage'],
            ['t1' => 'Cirsium arvense', 't2' => 'Cirse des champs'],
        ];

        if(! $fields )
            $fields = ['t1', 't2'];

        if( ! $args)
        $args = [
            'Petasites',
            'Cirse',
        ];

        $transformer = new RemoveTransformer( $datas, $fields, $args);

        return $transformer;
    }

    private function _make_basic_attempts()
    {
        $attempts = [
            ['t1' => ' hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage'],
            ['t1' => 'Cirsium arvense', 't2' => ' des champs'],
        ];
        return $attempts;
    }
}
