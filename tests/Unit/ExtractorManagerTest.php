<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class ExtractorManagerTest extends TestCase
{
    use RefreshDatabase;

    // config
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Etl\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    // tests
    public function testBasic()
    {
        $exist = \ExtractorManager::test_me();
        $this->assertTrue($exist);
    }

    public function testArrayExtractorRun()
    {
        $src = [
            'f01' => 'test1',
            'f02' => 'test2',
            'f03' => 'test3',
            'f04' => 'test4',
        ];

        $fields = [
            't1' => [
                'name' => 'f01',
                'rules'=> ['required']
            ],
            't2' => [
                'name' => 'f04',
                'rules'=> ['required']
            ],
        ];

        $attemps = [
            't1' => 'test1',
            't2' => 'test4',
        ];

        $datas = \ExtractorManager::run('array', $src, $fields);
        $this->assertEquals($attemps, $datas[0]);
    }

    public function testMultiArrayExtractorRun()
    {
        $src = [
            'r1' => [ 'a' => 'test-r1a', 'b'  => 'test-r1b', 'c' => 'test-r1c', 'd' => 'test-r1d'],
            'r2' => [ 'a' => 'test-r2a', 'b'  => 'test-r2b', 'c' => 'test-r2c', 'd' => 'test-r2d'],
            'r3' => [ 'a' => 'test-r3a', 'b'  => 'test-r3b', 'c' => 'test-r3c', 'd' => 'test-r3d'],
            'r4' => [ 'a' => 'test-r4a', 'b'  => 'test-r4b', 'c' => 'test-r4c', 'd' => 'test-r4d'],
            'r5' => [ 'a' => 'test-r5a', 'b'  => 'test-r5b', 'c' => 'test-r5c', 'd' => 'test-r5d'],
            'r6' => [ 'a' => 'test-r6a', 'b'  => 'test-r6b', 'c' => 'test-r6c', 'd' => 'test-r6d'],
        ];

        $fields = [
            't1' => [
                'name' => 'a',
                'rules'=> ['required']
            ],
            't2' => [
                'name' => 'c',
                'rules'=> ['required']
            ],
        ];

        $attemps = [
            ['t1' => 'test-r1a', 't2' => 'test-r1c'],
            ['t1' => 'test-r2a', 't2' => 'test-r2c'],
            ['t1' => 'test-r3a', 't2' => 'test-r3c'],
            ['t1' => 'test-r4a', 't2' => 'test-r4c'],
            ['t1' => 'test-r5a', 't2' => 'test-r5c'],
            ['t1' => 'test-r6a', 't2' => 'test-r6c'],
        ];

        $datas = \ExtractorManager::run('multi_array', $src, $fields);
        $this->assertEquals($attemps, $datas);
    }

    public function testCsvExtractorRun()
    {
        $src = __DIR__ . "/_files/test.csv";

        $fields = [
            't1' => [
                'name' => 1,
                'rules'=> ['required']
            ],
            't2' => [
                'name' => 2,
                'rules'=> ['required']
            ],
        ];

        $attemps = [
            ['t1' => 'Petasites hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage'],
            ['t1' => 'Cirsium arvense', 't2' => 'Cirse des champs'],
        ];

        $datas = \ExtractorManager::run('csv', $src, $fields);
        $this->assertEquals($attemps, $datas);
    }

    public function testExcelExtractorRun()
    {
        $src = __DIR__ . "/_files/test.xlsx";

        $fields = [
            't1' => [
                'name' => 1,
                'rules'=> ['required']
            ],
            't2' => [
                'name' => 2,
                'rules'=> ['required']
            ],
        ];

        $attempts = [
            ['t1' => 'Petasites hybridus', 't2' => 'Pétasite officinal'],
            ['t1' => 'Tussilago farfara', 't2' => 'Tussilage'],
            ['t1' => 'Cirsium arvense', 't2' => 'Cirse des champs'],
        ];

        $datas = \ExtractorManager::run('excel', $src, $fields);
        $this->assertEquals($attempts, $datas);
    }

    // -
}
