<?php

namespace Dendev\Etl\Services\Transformers;


class AmountTransformer extends ATransformer
{
    public function __construct($datas, $fields, $args = [])
    {
        parent::__construct($datas, $fields, $args);
    }

    protected function _transform($datas, $fields, $args)
    {
        $funct = function ($data, $key_row, $key_field, $args) use ( &$datas ){

            $credit_identity = ( is_array($args) && array_key_exists('credit_identity' , $args) ) ? $args['credit_identity'] : 'credit';
            $debit_identity  = ( is_array($args) && array_key_exists('debit_identity'  , $args) ) ? $args['debit_identity'] : 'debit';

            $amount = str_replace('+', '', $data);
            $amount = doubleval($amount);

            $value = str_replace('-', '', $amount);

            if( $amount < 0)
            {
                $datas[$key_row][$debit_identity] = $value;
                $datas[$key_row][$credit_identity] = null;
            }
            else
            {

                $datas[$key_row][$credit_identity] = $value;
                $datas[$key_row][$debit_identity] = null;
            }

            return $data;
        };

        return parent::_iterate_and_apply($datas, $funct, $fields, $args);
    }
}
