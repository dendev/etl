<?php

namespace Tests\Unit;

use Dendev\Etl\Services\Loaders\JsonLoader;
use Dendev\Etl\Services\Loaders\MultiArrayLoader;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class JsonLoaderTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Etl\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    public function testRun()
    {
        $json = '[{"1":"test 03","2":"test 01"},{"1":"test 13","2":"test 11"},{"1":"test 23","2":"test 21"}]';

        $loader = $this->_make_basic_loader();
        $datas = $loader->run();
        $this->assertEquals($json, $datas);
    }

    //
    private function _make_basic_loader($datas = false, $fields = false, $args = false)
    {
        if( ! $datas )
            $datas = [
                ['t1' => 'test 01', 't2' => 'test 02' , 't3' => 'test 03'],
                ['t1' => 'test 11', 't2' => 'test 12' , 't3' => 'test 13'],
                ['t1' => 'test 21', 't2' => 'test 22' , 't3' => 'test 23'],
            ];

        if( ! $fields)
            $fields = [
                't3' => [
                    'name' => '1',
                ],
                't1' => [
                    'name' => '2',
                ],
            ];

        if( ! $args)
            $args = [];

        $loader = new JsonLoader( $datas, $fields, $args);

        return $loader;
    }
}
