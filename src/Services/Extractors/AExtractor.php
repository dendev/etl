<?php


namespace Dendev\Etl\Services\Extractors;


/**
 * Utils and generic method to extract datas from src
 * Class AExtractor
 * @package Dendev\Etl\Services\Extractors
 */
abstract class AExtractor
{
    /**
     * AExtractor constructor.
     * @param $src
     * @param $fields
     * @param array $args
     */
    public function __construct($src, $fields, $args = [])
    {
        $this->_merge_args_and_defaults_values($args);
        $this->src = $src;
        $this->fields = $fields;
    }

    /**
     * Run all method to extract datas
     *
     * Valid src && field
     * Check src && get values from src
     *
     * @return mixed
     */
    public function run()
    {
        $values = [];

        // check
        $src_is_valid = $this->_check_src_is_valid();
        $fields_are_valid = $this->_check_fields_are_valid();

        // raw datas
        $datas = $this->_read_src_datas();

        // do or log
        if ($src_is_valid && $fields_are_valid)
            $values = $this->_get_values($datas);
        else if (!$src_is_valid)
            \Log::error("[Etl::AExtractor::run] Invalid src provided", ['src' => $this->src, 'src_is_valid' => $src_is_valid]);
        else
            \Log::error("[Etl::AExtractor::run] Invalid fields provided", ['src' => $this->src, 'fields_are_valid' => $fields_are_valid]);

        return $values;
    }

    /**
     *
     * Check if field respect rules
     *
     * rules are defined in field array for each field
     *
     * @param $field
     * @param $datas
     * @return bool
     */
    protected function _apply_field_rules($field, $datas)
    {
        $nb_validated_rules = 0;
        $is_valid = false;

        if( array_key_exists('rules', $field) && count( $field['rules'] ) > 0  )
        {
            $rules = $field['rules'];
            foreach( $rules as $rule )
            {
                // check rule
                if( $rule === 'required')
                    $is_valid_rule = $this->_apply_field_rule_required($field, $datas);

                // comptabilise
                if( $is_valid_rule )
                    $nb_validated_rules++;
            }

            // determine validity
            $nb_rules = count( $field['rules']);
            if( $nb_rules == $nb_validated_rules)
                $is_valid = true;
        }
        else
        {
            $is_valid = true; // not mandatory
        }

        return $is_valid;
    }

    /**
     * Field require his value exist in datas
     *
     * If value does not exist return false
     *
     * @param $field
     * @param $datas
     * @return bool
     */
    protected function _apply_field_rule_required($field, $datas)
    {
        $is_valid = false;

        $name = $field['name'];
        if( array_key_exists($name, $datas))
        {
            $is_valid = ( $datas[$name] ) ? true : false;
        }
        else
        {
            \Log::error("[Etl::ArrayExtractor::_apply_field_rule_required] Field $name is required but not found",['datas' => $datas]);
        }

        return $is_valid;
    }

    /**
     * Implemented by child
     *
     * Verifiy if source of datas is usable
     *
     * @return mixed
     */
    abstract protected function _check_src_is_valid();

    /**
     * Check if file exist and is readable
     *
     * @param $file_with_path
     * @return bool
     */
    protected function _check_file_is_valid($file_with_path)
    {
        $is_valid = false;

        $src = $this->src;
        if ($src && is_string($file_with_path))
        {
            if (file_exists($file_with_path) && is_readable($file_with_path))
            {
                $is_valid = true;
            }
        }

        return $is_valid;
    }

    /**
     * Check if fields arguments is correct
     *
     * a key name must exist for each field
     *
     * @return bool
     */
    protected function _check_fields_are_valid()
    {
        $is_valid = false;
        $fields = $this->fields;

        // check exist
        if( count( $fields ) > 0 )
        {
            // check each
            $nb_validated = 0;
            foreach ($fields as $field )
            {
                $is_valid_field = $this->_check_field_is_valid($field);
                if( $is_valid_field)
                    $nb_validated++;
            }

            // debug
            //dd( $nb_validated);

            // conclusion
            if( count( $fields) === $nb_validated )
                $is_valid = true;
        }

        return $is_valid;
    }

    /**
     * Check if field argument is correct
     *
     * a key name must exist
     *
     * @return bool
     */
    protected function _check_field_is_valid($field)
    {
        $is_valid = false;
        $is_valid_name = false;
        $is_valid_rules = false;

        // check each element
        //  //  name
        if( array_key_exists('name', $field) && isset($field['name']) && ( is_string($field['name']) || is_int($field['name'])))
        {
            $is_valid_name = true;
        }
        else
        {
            \Log::error("[Etl::AExtractor::_check_field_is_valid] name must exist and be an string or int",['field' => $field]);
        }

        //  //  rules
        if( array_key_exists('rules', $field) )
        {
            if ($field['rules'] && is_array($field['rules']))
            {
                $is_valid_rules = true;
            }
            else
            {
                \Log::error("[Etl::AExtractor::_check_field_is_valid] rule values must be an array",['rules' => $field['rules']]);
            }
        }
        else
        {
            $is_valid_rules = true; // rules not mandatory
        }

        // debug
        //dd( $is_valid_name);
        //dd( $is_valid_rules);

        // conclusion
        if( $is_valid_name && $is_valid_rules )
            $is_valid = true;

        return $is_valid;
    }

    /**
     * Return value of arg
     *
     * Args are merged and setted by _merge_args_and_defaults_values
     *
     * @see _merge_args_and_defaults_values
     * @param $key
     * @return mixed|null
     */
    protected function _get_arg_value( $key )
    {
        $value = null;
        $args = $this->args;

        if( array_key_exists($key, $args) )
        {
            $value = $args[$key];
        }

        return $value;
    }

    /**
     * Get value of field in datas
     *
     * Datas is an array ( associative of not ) who have a key equals field name
     *
     * @param $field
     * @param $datas
     * @return mixed|null
     */
    protected function _get_value_of_field($field, $datas)
    {
        $value = null;

        $name = $field['name'];
        if( array_key_exists($name, $datas))
        {
            $value = $datas[$name];
        }
        else
        {
            \Log::debug("[Etl::AExtractor::_get_value_of_field] Field $name not found in datas",['datas' => $datas ]);
        }

        return $value;
    }

    /**
     * Get all values for each fields
     *
     * @param $datas
     * @return mixed false if no values found or associative array of values
     */
    protected function _get_values($datas)
    {
        $values = [];
        $fields = $this->fields;

        $i = 0;
        foreach( $datas as $key_row => $data )
        {
            foreach ($fields as $key_field => $field)
            {
                // rules
                $is_valid = $this->_apply_field_rules($field, $data);
                if ($is_valid)
                {
                    // value
                    $value = $this->_get_value_of_field($field, $data);
                    $values[$i][$key_field] = $value;
                }
            }
            $i++;
        }

        return $values;
    }

    /**
     * Make args with default value and values of arguments args
     *
     * Set attribute args with merged array
     *
     * @param $args
     */
    protected function _merge_args_and_defaults_values($args)
    {
        $default = [
            'skip_first_row' => true,
        ];

        $merged = array_merge( $default, $args);

        $this->args = $merged;
    }

    /**
     * Implemented by child
     *
     * Way to get datas from src, must return an array of array with all datas
     *
     * @return mixed
     */
    abstract protected function _read_src_datas();

}
