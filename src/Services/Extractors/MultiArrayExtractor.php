<?php

namespace Dendev\Etl\Services\Extractors;

class MultiArrayExtractor extends AExtractor
{
    public function __construct($src, $fields, $args = [])
    {
        $this->src = $src;
        $this->fields = $fields;
        $this->args = $args;
    }

    protected function _check_src_is_valid()
    {
        $is_valid = false;

        $src = $this->src;
        if ($src && is_array($src) && count($src) > 0)
            $is_valid = true;

        return $is_valid;
    }

    protected function _read_src_datas()
    {
        $src = $this->src;
        $datas = false;

        $datas = $src;

        return $datas;
    }
}
