<?php

namespace Dendev\Etl\Facades;

use Illuminate\Support\Facades\Facade;

class ExtractorManagerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'extractor_manager';
    }
}
