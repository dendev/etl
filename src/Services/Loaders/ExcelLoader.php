<?php

namespace Dendev\Etl\Services\Loaders;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class ExcelLoader extends ALoader
{
    protected function _check_args_is_valid()
    {
        $is_valid = false;
        $args = $this->_args;

        if( is_array($args) && array_key_exists('type', $args) && array_key_exists('path', $args) && array_key_exists('filename', $args))
        {
            $path = $args['path'];

            if( is_dir($path) && is_writable($path) )
            {
                $is_valid = true;
            }
            else
            {
                \Log::error("[Etl::ExcelLoader::_check_args_is_valid] $path not writable directory", [
                    'path' => $path,
                    'args' => $args
                ]);
            }
        }
        else
        {
            \Log::error("[Etl::ExcelLoader::_check_args_is_valid] Missing arg type, path or filename", [
                'args' => $args
            ]);
        }

        return $is_valid;
    }

    protected function _write_datas($datas)
    {
        $args = $this->_args;

        $with_title = array_key_exists('with_title', $args) ? $args['with_title'] : false;

        // Settings
        $nb_cols = 0;
        if( count( $datas) > 0 && is_array($datas[0]) )
            $nb_cols = count( $datas[0]);

        //
        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();

        // Format
        // // columns
        $current_col = 'A';
        for( $c = 0; $c < $nb_cols; $c++ )
        {
            $spreadsheet->getActiveSheet()->getColumnDimension($current_col)->setAutoSize(true);
            $current_col++;
        }

        //  // title
        if( $with_title )
        {
            $current_col = 'A';
            $current_row = 1;
            $keys = array_keys($datas[0]);
            foreach ($keys as $key)
            {
                $current_cell = $current_col . $current_row;
                $sheet->setCellValue($current_cell, $key);

                $current_col++;
            }
        }

        //  // datas
        $col = 'A';
        $row = ( $with_title ) ? 2 : 1;
        foreach ($datas as $data )
        {
            foreach( $data as $key => $value )
            {
                $cell = $col . $row;
                $sheet->setCellValue($cell, $value);

                $col++;
            }
            $col = 'A';
            $row++;
        }

        // Create
        $writer = new Xlsx($spreadsheet);

        // Save
        $path_with_filename = $args['path'] . '/' . $args['filename'];
        $writer->save($path_with_filename);

        // End
        return true; // not really pertinent
    }
}

/*
 * ref: https://medium.com/ampersand-academy/php-read-and-write-excel-file-using-phpspreadsheet-ec0b41fb1fd0
 */
