<?php

namespace Dendev\Etl\Providers;

use Dendev\Etl\Services\LoaderManagerService;
use Illuminate\Support\ServiceProvider;

class LoaderServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('loader_manager', function ($app) {
            return new LoaderManagerService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
