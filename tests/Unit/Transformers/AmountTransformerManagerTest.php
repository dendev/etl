<?php

namespace Tests\Unit;

use Dendev\Etl\Services\Transformers\AmountToCreditOrDebitTransformer;
use Dendev\Etl\Services\Transformers\AmountTransformer;
use Dendev\Etl\Services\Transformers\UcfirstTransformer;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\TestCase;

class AmountTransformerTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    protected function getPackageProviders($app)
    {
        return [
            'Dendev\Etl\AddonServiceProvider',
            //'Backpack\CRUD\BackpackServiceProvider',
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = include './tests/config.php';
        $mysql_connection = $config['db']['mysql'];

        $app['config']->set('database.default', 'mysql');
        $app['config']->set('database.connections.mysql', $mysql_connection);
        $app['config']->set('auth.providers.users.model', 'App\Models\User');
    }

    public function testRun()
    {
        $attempts = $this->_make_basic_attempts();
        $transformer = $this->_make_basic_transformer();

        $transformed = $transformer->run();

        $this->assertArrayHasKey('Debit', $transformed[0]);
        $this->assertArrayHasKey('Credit', $transformed[1]);
        $this->assertArrayHasKey('Credit', $transformed[2]);

        $this->assertEquals($attempts[0]['Debit'],$transformed[0]['Debit']);
        $this->assertEquals($attempts[1]['Credit'],$transformed[1]['Credit']);
        $this->assertEquals($attempts[2]['Credit'],$transformed[2]['Credit']);
    }

    //
    private function _make_basic_transformer($datas = false, $fields = false, $args = false)
    {
        if( ! $datas )
            $datas = [
                ['t1' => '-10.2', 't2' => 'Lukoil'],
                ['t1' => '125.23', 't2' => 'Syndic'],
                ['t1' => '+75.65', 't2' => 'SD'],
            ];

        if( ! $fields )
            $fields = ['t1'];

        if( ! $args)
            $args = [
                'credit_identity' => 'Credit',
                'debit_identity' => 'Debit'
            ];

        $transformer = new AmountTransformer( $datas, $fields, $args);

        return $transformer;
    }

    private function _make_basic_attempts()
    {
        $attempts = [
            ['t1' => '-10.2', 't2' => 'Lukoil', 'Debit' => 10.2],
            ['t1' => '125.23', 't2' => 'Syndic', 'Credit' => 125.23],
            ['t1' => '+75.65', 't2' => 'SD', 'Credit' => 75.65],
        ];

        return $attempts;
    }
}
