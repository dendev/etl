<?php

namespace Dendev\Etl\Services\Transformers;


class ImplodeTransformer extends ATransformer
{
    public function __construct($datas, $fields, $args = [])
    {
        parent::__construct($datas, $fields, $args);
    }

    protected function _check_args_are_valid()
    {
        $is_valid = false;

        $args = $this->_args;

        if( is_array($args) )
        {
            $is_valid = true;
        }


        return $is_valid;
    }

    protected function _transform($datas, $fields, $args)
    {
        $fields = [];
        $separator = ',';
        $field = 'imploded';

        $implode = $args;

        if( array_key_exists('fields', $implode) && is_array($implode['fields']) )
            $fields = $implode['fields'];
        if( array_key_exists('separator', $implode) )
            $separator = $implode['separator'];
        if( array_key_exists('field', $implode) )
            $field_imploded = $implode['field'];

        foreach ($datas as $key => $data)
        {
            $founds = [];
            foreach( $implode['fields'] as $field )
            {
                // find fields to implode
                if (array_key_exists($field, $data))
                {
                    $founds[] = $data[$field];
                }
            }

            if( count( $founds) > 0 )
            {
                // implode
                $imploded = implode($separator, $founds);

                // save in datas
                $data[$field_imploded] = $imploded;
                $datas[$key] = $data;
            }
        }

        return $datas;
    }
}
